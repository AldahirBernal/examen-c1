package com.example.examenc1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RectanguloActivity extends AppCompatActivity {

    private TextView lblNombre;
    private EditText txtBase;
    private EditText txtAltura;
    private TextView lblArea;
    private TextView lblPerimetro;
    private Button btnCalcular;
    private Button btnRegreasar;
    private Button btnLimpiar;
    private Rectangulo rectangulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);

        lblNombre = (TextView) findViewById(R.id.lblNombre2);
        txtBase = (EditText) findViewById(R.id.txtBase);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        lblArea = (TextView) findViewById(R.id.lblArea);
        lblPerimetro = (TextView) findViewById(R.id.lblPerimetro);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnRegreasar = (Button) findViewById(R.id.btnRegresar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        rectangulo = new Rectangulo();

        Bundle bundle = getIntent().getExtras();

        String dato = getIntent().getStringExtra("nombre");
        lblNombre.setText("Bienvenido " + dato);


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String base = txtBase.getText().toString();
                String altura = txtAltura.getText().toString();
                if(base.matches("")){
                    Toast.makeText(RectanguloActivity.this,"Debe de ingresar una base",Toast.LENGTH_SHORT).show();
                }
                else if(altura.matches("")){
                    Toast.makeText(RectanguloActivity.this,"Debe de ingresar una altura",Toast.LENGTH_SHORT).show();
                }
                else{
                    int bas = Integer.parseInt(base);
                    int altu = Integer.parseInt(altura);
                    rectangulo.setBase(bas);
                    rectangulo.setAltura(altu);

                    String area = String.valueOf(rectangulo.calcularArea());
                    String perimetro = String.valueOf(rectangulo.calcularPerimetro());

                    lblArea.setText(area);
                    lblPerimetro.setText(perimetro);
                }
            }
        });

        btnRegreasar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtBase.setText("");
                txtAltura.setText("");
                lblArea.setText("");
                lblPerimetro.setText("");
            }
        });




    }
}
