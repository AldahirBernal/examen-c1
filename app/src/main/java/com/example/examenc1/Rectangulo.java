package com.example.examenc1;

import java.io.Serializable;

public class Rectangulo implements Serializable {

    private int Base;
    private int Altura;

    public Rectangulo(int base, int altura) {
        Base = base;
        Altura = altura;
    }

    public Rectangulo() {

    }

    public int getBase() {
        return Base;
    }

    public int getAltura() {
        return Altura;
    }

    public void setBase(int base) {
        Base = base;
    }

    public void setAltura(int altura) {
        Altura = altura;
    }

    public float calcularArea(){
        float area = 0;
        area = this.Base * this.Altura;
        return area;
    }

    public float calcularPerimetro(){
        float perimetro = 0;
        perimetro = this.Base + this.Base + this.Altura + this.Altura;
        return perimetro;
    }

}
